# def test_calling_functions_with_wrong_number_of_arguments(self):
#         try:
#             my_global_function(self)
#         except TypeError as exception:
#             msg = exception.args[0]

#         # Note, the text comparison works for Python 3.2
#         # It has changed in the past and may change in the future
#         self.assertRegex(msg,
#             r'my_global_function\(\) missing 2 required positional arguments')

#         try:
#             my_global_function(1, 2, 3)
#         except Exception as e:
#             msg = e.args[0]

#         # Note, watch out for parenthesis. They need slashes in front!
#         self.assertRegex(msg,'my_global_function\\(\\) missing 2 required positional arguments' )